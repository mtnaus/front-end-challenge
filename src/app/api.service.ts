import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) { }

  //Gets object with the death count total on the nearest date
  public getDeathCount() {
    return this.httpClient.get('https://covid19.richdataservices.com/rds/api/query/int/jhu_country/select?cols=cnt_death&where=(iso3166_1=US)&limit=1&orderBy=date_stamp%20DESC')
  }

  //Gets object with the confirmed count total on the nearest date
  public getConfirmedCount() {
    return this.httpClient.get('https://covid19.richdataservices.com/rds/api/query/int/jhu_country/select?cols=cnt_confirmed&where=(iso3166_1=US)&limit=1&orderBy=date_stamp%20DESC')
  }

}
