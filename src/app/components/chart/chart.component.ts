import { Component } from '@angular/core';
import { RdsServer, HttpResponse, AmchartsDataSet } from '@rds/sdk';
import { AMCHARTS_LINE_SERIES } from '../../shared/amcharts/amcharts-config'
import { AmChartsLineUtil } from '../../shared/amcharts/line-chart.util'

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})

export class AppChartComponent {
  title = 'Chart Component';

  ngOnInit() {
    //Uses the RDS sdk to make a call to get the deaths,recoverys, and total confirmed covid cases to put in an amcharts graph
    new RdsServer('https://covid19.richdataservices.com/rds')
      .getCatalog('int')
      .getDataProduct('jhu_country')
      .select<AmchartsDataSet>({
        cols: 'date_stamp,cnt_confirmed,cnt_death,cnt_recovered',
        where: '(iso3166_1=US)',
        metadata: true,
        limit: 5000,
        format: 'amcharts',
      })
      //Graph the data using some RDS utilities for amcharts
      .then((res: HttpResponse<AmchartsDataSet>) =>
        AmChartsLineUtil.createDateLineChart({
          elementId: 'chart-div',
          data: res.parsedBody && res.parsedBody.dataProvider,
          dateName: 'date_stamp',
          lines: AMCHARTS_LINE_SERIES,
          yTitle: 'Total Cases for U.S.',
        })
      )
  }
}
