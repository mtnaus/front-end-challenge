import { Component } from '@angular/core';
import { ApiService } from "../../api.service";



@Component({
  selector: 'app-info-box',
  templateUrl: './info-box.component.html',
  styleUrls: ['./info-box.component.scss']
})

export class AppInfoBoxComponent {
  title = 'Info Box Component';
  confirmed: any
  confirmedClean: any
  deaths: any
  deathsClean: any
  survivalRate: any;

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    //Use service to make API calls
    this.apiService.getDeathCount().subscribe((dData) => {
      this.deaths = Object.values(dData)[1]
      this.apiService.getConfirmedCount().subscribe((cData) => {
        this.confirmed = Object.values(cData)[1]

        //Add commas to the string to make it look nicer
        this.confirmedClean = this.numberWithCommas(this.confirmed);
        this.deathsClean = this.numberWithCommas(this.deaths);

        //Calculate the survival rate of Covid in the US
        this.survivalRate = this.calcSurvivalRate(this.deaths, this.confirmed);

      });
    });


  }
  numberWithCommas(x: string) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  calcSurvivalRate(num: string, denom: string) {
    //Convert to number
    var intNum = parseInt(num, 10)
    var intDenom = parseInt(denom, 10)

    //This swaps the percentage around to show survival rate instead of death rate.
    var newSurvivalRate = 100 - (intNum / intDenom)

    return newSurvivalRate.toFixed(2)
  }
}
